from django.shortcuts import redirect, render
from django.http import HttpResponse, HttpResponseRedirect
from .forms import Input_form
from .models import mataKuliah,chooseMatkul

respons={}

def home(request):
    return render(request, 'main/home.html')

def project(request):
    return render(request, 'main/project.html')

def thankyou(request):
    return render(request, 'main/thankyou.html')

# views buat story 5
def addSchedule(request):
    if (request.user.is_anonymous):
        return redirect('/auth/login')

    form=Input_form(request.POST or None)
    if (form.is_valid() and request.method=='POST'):
        form.save()
        return HttpResponseRedirect('/listmatkul')
    respons['input_form']=form
    return render(request, 'main/addSchedule.html',respons)

def listmatkul(request):
    if (request.user.is_anonymous):
        return redirect('/auth/login')

    objek=mataKuliah.objects.all().order_by('nama_matkul')
    respons['objek']=objek
    return render(request,'main/listmatkul.html',respons)

def hapus(request,kode):
    if (request.user.is_anonymous):
        return redirect('/auth/login')

    kode = int(kode)
    if request.method=='POST':
        namaMatkul = mataKuliah.objects.get(id = kode)
        namaMatkul.delete()
        return HttpResponseRedirect('/listmatkul')
    return render(request,'main/hapus.html')

def update(request,kode):
    if (request.user.is_anonymous):
        return redirect('/auth/login')

    kode=int(kode)
    Matkul = mataKuliah.objects.get(id = kode)
    form=Input_form(request.POST or None,instance=Matkul)
    if (request.method=='POST' and form.is_valid()):
        form.save()
        return HttpResponseRedirect('/listmatkul')
    respons['input_form']=form
    return render(request, 'main/update.html',respons)


def details(request,kode):
    if (request.user.is_anonymous):
        return redirect('/auth/login')
        
    kode=int(kode)
    Matkul = mataKuliah.objects.get(id = kode)
    respons['matkul']=Matkul
    return render(request,'main/details.html',respons)
# closing #
# sumber dari https://data-flair.training/blogs/django-crud-example/





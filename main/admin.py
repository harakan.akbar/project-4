from django.contrib import admin
from .models import mataKuliah, chooseMatkul

# Register your models here.
admin.site.register(mataKuliah)
admin.site.register(chooseMatkul)

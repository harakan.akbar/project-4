from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('project', views.project, name='project'),
    path('thankyou', views.thankyou, name='thankyou'),
    path('addschedule',views.addSchedule,name='addschedule'),
    path('listmatkul',views.listmatkul,name='listmatkul'),
    path('delete/<str:kode>',views.hapus,name='hapus'),
    path('edit/<str:kode>',views.update,name='update'),
    path('detail/<str:kode>',views.details,name='details'),
]


from django import forms
from django.forms import ModelForm
from .models import mataKuliah,chooseMatkul

# Isi form mu disini

# form story 5 #
class Input_form(ModelForm):
    class Meta:
        model=mataKuliah
        fields=[
            'nama_matkul',
            'dosen_pengajar',
            'jumlah_sks',
            'deskripsi',
            'semesterTahun',
            'ruangKelas',
            'tugas'
        ]

    nama_matkul=forms.CharField(label='',required=True,max_length=70,widget=forms.TextInput(attrs={
                                'class': 'form-control form-control-sm','id':'matkul','placeholder':'Mata Kuliah'}))
    dosen_pengajar=forms.CharField(label='',required=True,max_length=50,widget=forms.TextInput(attrs={
                                'class': 'form-control','id':'dosen','placeholder':'Nama Dosen'}))
    jumlah_sks=forms.IntegerField(label='',required=True,min_value=0,widget=forms.NumberInput(attrs={
                                'class': 'form-control','id':'sks'}))
    deskripsi=forms.CharField(label='',required=True,max_length=300,widget=forms.Textarea(attrs={
                                'class': 'form-control','id':'deskripsi'}))

    PILIHAN=(
       ('Gasal 2019/2020', 'Gasal 2019/2020'),
        ('Genap 2019/2020', 'Genap 2019/2020'),
        ('Gasal 2020/2021', 'Gasal 2020/2021'),
        ('Genap 2020/2021', 'Genap 2020/2021'),
        ('Gasal 2020/2021', 'Gasal 2020/2021'),
    )
    semesterTahun=forms.ChoiceField(label='',required=True,choices=PILIHAN)
    ruangKelas=forms.IntegerField(label='',required=True,min_value=0,widget=forms.NumberInput(attrs={'class': 'form-control','id':'ruangan'}))
    tugas=forms.CharField(label='',required=True,max_length=300,widget=forms.Textarea(attrs={'class': 'form-control','id':'tugas'}))
## closing ##




        


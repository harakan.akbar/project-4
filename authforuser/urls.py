from django.urls import path

from . import views

app_name = 'authforuser'

urlpatterns = [
    path('login', views.logIn, name = 'login'),
    path('register', views.register, name = 'register'),
    path('logout', views.auth_logout, name = 'logout'),
]


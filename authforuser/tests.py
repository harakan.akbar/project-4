from http import HTTPStatus

from django.test import TestCase,Client
from django.urls import resolve, reverse
from django.apps import apps

from .views import logIn,register
from .apps import AuthforuserConfig
from django.contrib.auth.models import User


# Create your tests here.
class AuthForUserTC(TestCase):
    # fungsi app
    def test_app(self):
        self.assertEqual(AuthforuserConfig.name,'authforuser')
        self.assertEqual(apps.get_app_config('authforuser').name,'authforuser')

    # fungsi GET
    def test_login_GET_if_already_login(self):
        new_account = User.objects.create_user('akbar', 'akbar@gmail.com', 'halo')
        new_account.set_password('halo')
        new_account.save()
        c= Client()
        login= c.login(username=new_account.username, password='halo')
        response= c.get('/auth/login')
        self.assertEqual(response['location'],'/')
    
    def test_register_GET_if_already_login(self):
        new_account = User.objects.create_user('akbar', 'akbar@gmail.com', 'halo')
        new_account.set_password('halo')
        new_account.save()
        c= Client()
        login= c.login(username=new_account.username, password='halo')
        response= c.get('/auth/register')
        self.assertEqual(response['location'],'/')
    
    def test_logout_GET_if_not_login(self):
        c= Client()
        response= c.get('/auth/logout')
        self.assertEqual(response['location'],'/auth/login')

    def test_login_GET(self):
        c=Client()
        response= c.get('/auth/login')
        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertTemplateUsed(response,'main/login.html')
    
    def test_register_GET(self):
        c=Client()
        response= c.get('/auth/register')
        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertTemplateUsed(response,'main/register.html')
    
    # fungsi POST
    def test_login_POST_can_login(self):
        new_account = User.objects.create_user('akbar', 'akbar@gmail.com', 'halo')
        new_account.set_password('halo')
        new_account.save()

        c= Client()
        response=c.post(
            '/auth/login',
            data={
                'user_name_login' : 'akbar',
                'user_password_login' : 'halo',
            }
        )
        self.assertEqual(response['location'],'/')

    def test_login_POST_cannot_login(self):
        new_account = User.objects.create_user('akbar', 'akbar@gmail.com', 'halo')
        new_account.set_password('halo')
        new_account.save()

        c= Client()
        response=c.post(
            '/auth/login',
            data={
                'user_name_login' : 'nov',
                'user_password_login' : 'von',
            }
        )
        self.assertTemplateUsed(response,'main/login.html')
        html_response = response.content.decode('utf8')
        self.assertIn('Maaf password atau username salah', html_response)
    
    def test_register_POST_can_register(self):
        c= Client()
        response=c.post(
            '/auth/register',
            data={
                'user_name_register' : 'akbar',
                'user_email_register' : 'akbar@gmail.com',
                'user_password_register' : 'halo',
            }
        )
        self.assertEqual(response['location'],'/')

    def test_register_POST_cannot_register_same_username(self):
        new_account = User.objects.create_user('akbar', 'akbar@gmail.com', 'halo')
        new_account.set_password('halo')
        new_account.save()

        c= Client()
        response=c.post(
            '/auth/register',
            data={
                'user_name_register' : 'akbar',
                'user_email_register' : 'aidi@gmail.com',
                'user_password_register' : 'halo',
            }
        )
        self.assertTemplateUsed(response,'main/register.html')
        html_response = response.content.decode('utf8')
        self.assertIn('Maaf username sudah ada', html_response)

    def test_register_POST_cannot_register_same_email(self):
        new_account = User.objects.create_user('akbar', 'akbar@gmail.com', 'halo')
        new_account.set_password('halo')
        new_account.save()

        c= Client()
        response=c.post(
            '/auth/register',
            data={
                'user_name_register' : 'aidi',
                'user_email_register' : 'akbar@gmail.com',
                'user_password_register' : 'halo',
            }
        )
        self.assertTemplateUsed(response,'main/register.html')
        html_response = response.content.decode('utf8')
        self.assertIn('Maaf email sudah ada', html_response)
    
    def test_register_POST_cannot_register_same_email_and_username(self):
        new_account = User.objects.create_user('akbar', 'akbar@gmail.com', 'halo')
        new_account.set_password('halo')
        new_account.save()

        c= Client()
        response=c.post(
            '/auth/register',
            data={
                'user_name_register' : 'akbar',
                'user_email_register' : 'akbar@gmail.com',
                'user_password_register' : 'halo',
            }
        )
        self.assertTemplateUsed(response,'main/register.html')
        html_response = response.content.decode('utf8')
        self.assertIn('Maaf username dan email sudah ada', html_response)
    
    def test_logout_POST_can_logout(self):
        new_account = User.objects.create_user('akbar', 'akbar@gmail.com', 'halo')
        new_account.set_password('halo')
        new_account.save()
        c= Client()
        login= c.login(username=new_account.username, password='halo')
        response= c.get('/auth/logout')
        self.assertEqual(response['location'],'/')






























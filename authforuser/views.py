from django.shortcuts import render
from django.shortcuts import redirect, render, resolve_url
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User 

respons={}

# Create your views here.
def logIn(request):
    if (not request.user.is_anonymous):
        return redirect('/')
    
    respons['error']=False
    if (request.method== 'POST'):
        name= request.POST.get('user_name_login',False)
        password=request.POST.get('user_password_login',False)

        user = authenticate (
            request,
            username= name,
            password= password,
        )

        if (user != None ):
            login(request, user)
            return redirect('/')

        else:
            respons['error'] =True
            respons['isiError'] = 'Maaf password atau username salah'
            return render (request, 'main/login.html', respons)

    return render (request, 'main/login.html', respons)

def register(request):
    if (not request.user.is_anonymous):
    	return redirect('/')

    respons['error']=False

    if (request.method == 'POST'):
        name= request.POST.get('user_name_register',False)
        email=request.POST.get('user_email_register',False)
        password=request.POST.get('user_password_register',False)

        try:
            account = User.objects.get(username = name)
        except:
            account = None

        try:
            accountEmail = User.objects.get( email= email)
        except:
            accountEmail = None

        if (account == None and accountEmail == None):
            new_account = User.objects.create_user(name,email,password)
            new_account.save()
            login(request, new_account)
            return redirect ('/')

        elif (account != None and accountEmail == None):
            respons['error'] = True
            respons['isiError']='Maaf username sudah ada'

        elif (account == None and accountEmail != None):
            respons['error']=True
            respons['isiError']= 'Maaf email sudah ada'

        else:
            respons['errorUsername']=True
            respons['error']=True
            respons['isiError']='Maaf username dan email sudah ada'

    return render (request,'main/register.html',respons)

def auth_logout(request):
    if (request.user.is_anonymous):
        return redirect('/auth/login')
    else:
        logout(request)
        return redirect ('/')

from django import forms
from django.forms import ModelForm
from .models import Event, User_Event


## form untuk story 6 ##
class form_event(ModelForm):
    class Meta:
        model=Event
        fields=[
            'nama_event',
            'slogan',
            'deskripsi_event'
        ]
    
    nama_event= forms.CharField(label='',required=True,max_length=70,widget=forms.TextInput (attrs={
                            'class': 'form-control form-control-sm','id':'namaEvent','placeholder':'Nama Event Kamu'}))

    slogan=forms.CharField(label='',required=True,max_length=300,widget=forms.Textarea(attrs={
                             'class': 'form-control','id':'slogan'}))
    
    deskripsi_event=forms.CharField(label='',required=True,max_length=500,widget=forms.Textarea(attrs={
                             'class': 'form-control','id':'deskripsiEvent'}))


class form_user(ModelForm):
    class Meta:
        model=User_Event
        fields=[
            'nama_user'
        ]

    nama_user=forms.CharField(label='',required=True,max_length=70,widget=forms.TextInput (attrs={
                            'class': 'form-control form-control-sm','id':'namaUser','placeholder':'Nama Kamu'}))

                
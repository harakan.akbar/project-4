from django.urls import path

from . import views

app_name = 'story6'

urlpatterns = [
    path('addEvent', views.addEvent, name='addEvent'),
    path('addUser/<str:kode>', views.addUser, name='addUser'),
    path('deleteEvent/<str:kode>', views.deleteEvent, name='deleteEvent'),
    path('detailEvent/<str:nama>', views.detailEvent, name='detailEvent'),
    path('listEvent', views.listEvent, name='listEvent'),
]

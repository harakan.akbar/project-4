from django.db import models

# Create your models here.

# models for story 6
class Event(models.Model):
    nama_event=models.CharField(max_length=70)
    slogan=models.TextField(max_length=300, null=True)
    deskripsi_event=models.TextField(max_length=500, null=True)
    def __str__(self):
        return self.nama_event

class User_Event(models.Model):
    event=models.ForeignKey(Event, on_delete=models.CASCADE)
    nama_user=models.CharField(max_length=70)
    def __str__(self):
        return self.nama_user
## closing ##

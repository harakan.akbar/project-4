from http import HTTPStatus

from django.apps import apps
from django.test import Client, TestCase
from django.urls import resolve, reverse
from django.contrib.auth.models import User

from .apps import Story6Config
from .models import Event, User_Event
from .views import addEvent, addUser, deleteEvent, detailEvent, listEvent

# buat test mu disini

class Story6Test(TestCase):
    # test apps
    def test_apps(self):
        self.assertEqual(Story6Config.name, 'story6')
        self.assertEqual(apps.get_app_config('story6').name, 'story6')

    # test models
    def test_model_event_str(self):
        object=Event.objects.create(
            nama_event='Sunatan',
            slogan='sunat ampe mulus',
            deskripsi_event='Bagus sunatnya',
        )
        self.assertEqual(object.__str__(),'Sunatan')
    
    def test_model_user_str(self):
        object_event=Event.objects.create(
            nama_event='Sunatan',
            slogan='sunat ampe mulus',
            deskripsi_event='Bagus sunatnya',
        )
        object=User_Event.objects.create(
            event=object_event,
            nama_user='Ali Muhammad'
        )
        self.assertEqual(object.__str__(),'Ali Muhammad')
    
    # test view
    def test_listEvent_function(self):
        found=resolve('/listEvent')
        self.assertEqual(found.func,listEvent)
    
    def test_addEvent_function(self):
        found=resolve('/addEvent')
        self.assertEqual(found.func,addEvent)
    
    def test_deleteEvent_function(self):
        object=Event.objects.create(
            nama_event='Sunatan',
            slogan='sunat ampe mulus',
            deskripsi_event='Bagus sunatnya',
        )
        url='/deleteEvent/%d' % object.id 
        found=resolve(url)
        self.assertEqual(found.func,deleteEvent)

    def test_detailEvent_function(self):
        object=Event.objects.create(
            nama_event='Sunatan',
            slogan='sunat ampe mulus',
            deskripsi_event='Bagus sunatnya',
        )
        url='/detailEvent/%d' % object.id 
        found=resolve(url)
        self.assertEqual(found.func,detailEvent)
    
    def test_addUser_function(self):
        object=Event.objects.create(
            nama_event='Sunatan',
            slogan='sunat ampe mulus',
            deskripsi_event='Bagus sunatnya',
        )
        url='/addUser/%d' % object.id 
        found=resolve(url)
        self.assertEqual(found.func,addUser)

    def test_addEvent_POST_canAddEvent(self):
        new_account = User.objects.create_user('akbar', 'akbar@gmail.com', 'halo')
        new_account.set_password('halo')
        new_account.save()
        c= Client()
        login= c.login(username=new_account.username, password='halo')
        response=c.post(
            '/addEvent',
            data={
                'nama_event' : 'budy',
                'slogan' : 'asoygeboy',
                'deskripsi_event': 'Mantab Jiwa',
            }
        )
        self.assertEqual(Event.objects.all().count(),1)
        self.assertEqual(response.status_code, 302)
    
    def test_addEvent_POST_cannotAddEvent_without_login(self):
        c= Client()
        response=c.post(
            '/addEvent',
            data={
                'nama_event' : 'budy',
                'slogan' : 'asoygeboy',
                'deskripsi_event': 'Mantab Jiwa',
            }
        )
        self.assertEqual(response['location'],'/auth/login')

    def test_addUser_POST_canAddUser(self):
        new_account = User.objects.create_user('akbar', 'akbar@gmail.com', 'halo')
        new_account.set_password('halo')
        new_account.save()
        c= Client()
        login= c.login(username=new_account.username, password='halo')
        object_event=Event.objects.create(
            nama_event='Sunatan',
            slogan='sunat ampe mulus',
            deskripsi_event='Bagus sunatnya',
        )
        url='/addUser/%d' % object_event.id
        response=c.post(
            url,
            data={
                'event' : object_event,
                'nama_user' : 'Bitzy',
            }
        )
        self.assertEqual(User_Event.objects.all().filter(event=object_event).count(),1)
        self.assertEqual(response.status_code, 302)
    
    def test_addUser_POST_cannotAddUser_without_login(self):
        c= Client()
        object_event=Event.objects.create(
            nama_event='Sunatan',
            slogan='sunat ampe mulus',
            deskripsi_event='Bagus sunatnya',
        )
        url='/addUser/%d' % object_event.id
        response=c.post(
            url,
            data={
                'event' : object_event,
                'nama_user' : 'Bitzy',
            }
        )
        self.assertEqual(response['location'],'/auth/login')

    def test_deleteEvent_POST_canDelete(self):
        new_account = User.objects.create_user('akbar', 'akbar@gmail.com', 'halo')
        new_account.set_password('halo')
        new_account.save()
        c= Client()
        login= c.login(username=new_account.username, password='halo')
        response=c.post(
            '/addEvent',
            data={
                'nama_event' : 'budy',
                'slogan' : 'asoygeboy',
                'deskripsi_event': 'Mantab Jiwa',
            }
        )
        self.assertEqual(Event.objects.all().count(),1)
        response=c.post(
            '/deleteEvent/1'
        )
        self.assertEqual(Event.objects.all().count(),0)
        self.assertEqual(response.status_code, 302)

    def test_deleteEvent_POST_cannotDelete_without_login(self):
        new_account = User.objects.create_user('akbar', 'akbar@gmail.com', 'halo')
        new_account.set_password('halo')
        new_account.save()
        c= Client()
        login= c.login(username=new_account.username, password='halo')
        response=c.post(
            '/addEvent',
            data={
                'nama_event' : 'budy',
                'slogan' : 'asoygeboy',
                'deskripsi_event': 'Mantab Jiwa',
            }
        )
        self.assertEqual(Event.objects.all().count(),1)
        logout=c.logout()
        response=c.post(
            '/deleteEvent/1'
        )
        self.assertEqual(response['location'],'/auth/login')

    def test_listEvent_GET(self):
        new_account = User.objects.create_user('akbar', 'akbar@gmail.com', 'halo')
        new_account.set_password('halo')
        new_account.save()
        c= Client()
        login= c.login(username=new_account.username, password='halo')
        response= c.get('/listEvent')
        self.assertTemplateUsed(response,'main/listEvent.html')
        self.assertEqual(response.status_code, HTTPStatus.OK)
    
    def test_listEvent_without_GET_login(self):
        c= Client()
        response= c.get('/listEvent')
        self.assertEqual(response['location'],'/auth/login')

    def test_addEvent_GET(self):
        new_account = User.objects.create_user('akbar', 'akbar@gmail.com', 'halo')
        new_account.set_password('halo')
        new_account.save()
        c= Client()
        login= c.login(username=new_account.username, password='halo')
        response=c.get('/addEvent')
        self.assertTemplateUsed(response,'main/addEvent.html')
        self.assertEqual(response.status_code, HTTPStatus.OK)
    
    def test_addEvent_GET_without_login(self):
        c= Client()
        response=c.get('/addEvent')
        self.assertEqual(response['location'],'/auth/login')

    def test_detailEvent_GET(self):
        new_account = User.objects.create_user('akbar', 'akbar@gmail.com', 'halo')
        new_account.set_password('halo')
        new_account.save()
        c= Client()
        login= c.login(username=new_account.username, password='halo')
        object_event=Event.objects.create(
            nama_event='Sunatan',
            slogan='sunat ampe mulus',
            deskripsi_event='Bagus sunatnya',
        )
        nama=Event.objects.get(id=1).nama_event
        response= c.get('/detailEvent/%s' % nama)
        self.assertTemplateUsed(response,'main/detailEvent.html')
        self.assertEqual(response.status_code, HTTPStatus.OK)
    
    def test_detailEvent_GET_without_login(self):
        new_account = User.objects.create_user('akbar', 'akbar@gmail.com', 'halo')
        new_account.set_password('halo')
        new_account.save()
        c= Client()
        login= c.login(username=new_account.username, password='halo')
        object_event=Event.objects.create(
            nama_event='Sunatan',
            slogan='sunat ampe mulus',
            deskripsi_event='Bagus sunatnya',
        )
        logout=c.logout()
        nama=Event.objects.get(id=1).nama_event
        response= c.get('/detailEvent/%s' % nama)
        self.assertEqual(response['location'],'/auth/login')

    def test_deleteEvent_GET(self):
        new_account = User.objects.create_user('akbar', 'akbar@gmail.com', 'halo')
        new_account.set_password('halo')
        new_account.save()
        c= Client()
        login= c.login(username=new_account.username, password='halo')
        object_event=Event.objects.create(
            nama_event='Sunatan',
            slogan='sunat ampe mulus',
            deskripsi_event='Bagus sunatnya',
        )
        response= c.get('/deleteEvent/%d' % object_event.id)
        self.assertTemplateUsed(response,'main/deleteEvent.html')
        self.assertEqual(response.status_code, HTTPStatus.OK)

    def test_addUser_GET(self):
        new_account = User.objects.create_user('akbar', 'akbar@gmail.com', 'halo')
        new_account.set_password('halo')
        new_account.save()
        c= Client()
        login= c.login(username=new_account.username, password='halo')
        object_event=Event.objects.create(
            nama_event='Sunatan',
            slogan='sunat ampe mulus',
            deskripsi_event='Bagus sunatnya',
        )
        response= c.get('/addUser/%d' % object_event.id)
        self.assertTemplateUsed(response,'main/addUser.html')
        self.assertEqual(response.status_code, HTTPStatus.OK)
    



    



from django.shortcuts import render
from django.shortcuts import redirect, render
from django.http import HttpResponse, HttpResponseRedirect
from .forms import form_event,form_user
from .models import Event,User_Event

# Create your views here.

#views untuk story 6

respons={}
def addEvent(request):
    if (request.user.is_anonymous):
        return redirect('/auth/login')

    form=form_event(request.POST or None)
    if (form.is_valid() and request.method=='POST'):
        form.save()
        return redirect('/listEvent')
    respons['input_form']=form
    return render(request, 'main/addEvent.html',respons)

def listEvent(request):
    if (request.user.is_anonymous):
        return redirect('/auth/login')

    objek=Event.objects.all().order_by('nama_event')
    respons['objek']=objek
    return render(request,'main/listEvent.html',respons)

def detailEvent(request, nama):
    if (request.user.is_anonymous):
        return redirect('/auth/login')

    nama=str(nama)
    event=Event.objects.all().filter(nama_event=nama)
    user=User_Event.objects.all().filter(event_id=event[0].id).order_by('nama_user')
    respons['event']=event
    respons['user']=user
    return render(request,'main/detailEvent.html',respons)

def deleteEvent(request, kode):
    if (request.user.is_anonymous):
        return redirect('/auth/login')

    kode = int(kode)
    if request.method=='POST':
        event = Event.objects.get(id = kode)
        event.delete()
        return redirect('/listEvent')
    return render(request,'main/deleteEvent.html')

def addUser(request, kode):
    if (request.user.is_anonymous):
        return redirect('/auth/login')
        
    event=Event.objects.get(id=kode)
    form=form_user(request.POST or None)
    if (form.is_valid() and request.method=='POST'):
        user=form.save(commit=False)
        user.event=event
        user.save()
        return redirect('/detailEvent/{}'. format(event))
    respons['input_form']=form
    return render(request,'main/addUser.html',respons)

    


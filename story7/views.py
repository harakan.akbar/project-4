from django.shortcuts import redirect, render

# Create your views here.

def accordion(request):
    if (request.user.is_anonymous): return redirect('/auth/login')
       
        
    return render(request,'main/story7.html')

$(document).ready(function() {
    var icons = {
        header: "ui-icon-circle-arrow-e",
        activeHeader: "ui-icon-circle-arrow-s"
    };
    $("#accordion")
        .accordion({
            header: "> div > h3",
            icons: icons,
            collapsible: true
        })
        .sortable({
            axis: "y",
            handle: "h3",
            stop: function(event, ui) {
                // IE doesn't register the blur when sorting
                // so trigger focusout handlers to remove .ui-state-focus
                ui.item.children("h3").triggerHandler("focusout");

                // Refresh accordion to handle new order
                $(this).accordion("refresh");
            }
        });

    $(".button-up").click(function(e) {
        const currentItem = $(this).parent().parent();
        currentItem.insertBefore(currentItem.prev()) // pindahkan item ke atas(sebelum) item di atasnya
        e.preventDefault();
        e.stopPropagation();
    });

    $(".button-down").click(function(e) {
        const currentItem = $(this).parent().parent();
        currentItem.insertAfter(currentItem.next()) // pindahkan item ke bawah(sesudah) item di bawahnya
        e.preventDefault();
        e.stopPropagation();
    });
});

// documentation
// https://api.jquery.com/insertbefore/
from http import HTTPStatus

from django.test import TestCase,Client
from django.urls import resolve, reverse
from django.apps import apps
from django.contrib.auth.models import User

from .views import accordion
from .apps import Story7Config
# Create your tests here.

class Story7TestUnit(TestCase):
    def test_apps(self):
        self.assertEqual(Story7Config.name,'story7')
        self.assertEqual(apps.get_app_config('story7').name,'story7')
    
    def test_url_activity_GET(self):
        new_account = User.objects.create_user('akbar', 'akbar@gmail.com', 'halo')
        new_account.set_password('halo')
        new_account.save()
        c= Client()
        login= c.login(username=new_account.username, password='halo')
        response=c.get('/story7/aboutme')
        self.assertTemplateUsed(response,'main/story7.html')
        self.assertEqual(response.status_code,HTTPStatus.OK)
    
    def test_url_activity_GET_without_login(self):
        c= Client()
        response=c.get('/story7/aboutme')
        self.assertEqual(response['location'],'/auth/login')

    def test_url_func_GET(self):
        found=resolve('/story7/aboutme')
        self.assertEqual(found.func,accordion)

        


    




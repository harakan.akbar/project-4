$(document).ready(function() {
    $.ajax({
        url: "/story8/hasil?q=" + "Struktur Data",
        success: function(data) {
            $(".result").empty();
            var array = data.items;
            // console.log(array); for debug
            for (i = 0; i < array.length; i++) {
                var judul = array[i].volumeInfo.title;
                var gambar = array[i].volumeInfo.imageLinks.smallThumbnail;
                var author = array[i].volumeInfo.authors;
                var categories = array[i].volumeInfo.categories;
                var link = array[i].volumeInfo.infoLink;
                $(".result").append(`
                <div class="card mb-3">
                <div class="row no-gutters">
                    <div class="col-md-4">
                    <img src=` + gambar + ` class="img-thumbnail" alt="...">
                    </div>
                    <div class="col-md-4">
                    <div class="card-body">
                        <h5 class="card-title">` + judul + `</h5>
                        <p class="card-text">` + author + `</p>
                        <p class="card-text"> Categorie : ` + categories + `</p>
                        <a href=` + link + ` class="btn btn-primary"> Link GoogleBook </a>
                    </div>
                    </div>
                </div>
                `);
            }
        }
    });

    $("#search").on('keyup change', function() {
        var input = $("#search").val();
        // console.log(input); for debug
        // AJAX GAN
        if (input == "") {
            input = "Struktur Data"
        }
        $.ajax({
            url: "/story8/hasil?q=" + input,
            success: function(data) {
                $(".result").empty();
                var array = data.items;
                // console.log(array); for debug
                for (i = 0; i < array.length; i++) {
                    var judul = array[i].volumeInfo.title;
                    var gambar = array[i].volumeInfo.imageLinks.smallThumbnail;
                    var author = array[i].volumeInfo.authors;
                    var categories = array[i].volumeInfo.categories;
                    var link = array[i].volumeInfo.infoLink;
                    $(".result").append(`
                    <div class="card mb-3">
                    <div class="row no-gutters">
                        <div class="col-md-4">
                        <img src=` + gambar + ` class="img-thumbnail" alt="...">
                        </div>
                        <div class="col-md-4">
                        <div class="card-body">
                            <h5 class="card-title">` + judul + `</h5>
                            <p class="card-text">` + author + `</p>
                            <p class="card-text"> Categorie : ` + categories + `</p>
                            <a href=` + link + ` class="btn btn-primary"> Link GoogleBook </a>
                        </div>
                        </div>
                    </div>
                    `);
                }
            }
        });
    });
});
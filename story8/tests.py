from http import HTTPStatus

from django.test import TestCase,Client
from django.urls import resolve, reverse
from django.apps import apps
from django.contrib.auth.models import User 

from .apps import Story8Config
from .views import landing_story8,hasil

# Create your tests here.
class Story8TestUnit(TestCase):
    def test_apps(self):
        self.assertEqual(Story8Config.name,'story8')
        self.assertEqual(apps.get_app_config('story8').name,'story8')

    def test_url_activity_GET(self):
        new_account = User.objects.create_user('akbar', 'akbar@gmail.com', 'halo')
        new_account.set_password('halo')
        new_account.save()
        c= Client()
        login= c.login(username=new_account.username, password='halo')
        response= c.get('/story8/searchBook')
        self.assertTemplateUsed(response,'main/story8.html')
        self.assertEqual(response.status_code,HTTPStatus.OK)

    def test_url_activity_GET_without_login(self):
        c=Client()
        response= c.get('/story8/searchBook')
        self.assertEqual(response['location'],'/auth/login')

    def test_url_func_GET(self):
        found=resolve('/story8/searchBook')
        self.assertEqual(found.func,landing_story8)
    
    def test_url_hasil_func_GET(self):
        found=resolve('/story8/hasil/')
        self.assertEqual(found.func,hasil)

    def test_hasil_search_book_can_search(self):
        new_account = User.objects.create_user('akbar', 'akbar@gmail.com', 'halo')
        new_account.set_password('halo')
        new_account.save()
        c= Client()
        login= c.login(username=new_account.username, password='halo')
        response= c.get("/story8/hasil?q=frozen")
        self.assertEqual(response.status_code,301)

    def test_hasil_landing_has_content(self):
        new_account = User.objects.create_user('akbar', 'akbar@gmail.com', 'halo')
        new_account.set_password('halo')
        new_account.save()
        c= Client()
        login= c.login(username=new_account.username, password='halo')
        response= c.get("/story8/hasil/")
        self.assertEqual(response.status_code,200)
    
    def test_hasil_landing_no_login(self):
        c=Client()
        response=c.get("/story8/hasil/")
        self.assertEqual(response['location'],'/auth/login')

    



    


    
    

    
    


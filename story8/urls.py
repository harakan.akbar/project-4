from django.urls import path

from . import views

app_name = 'story8'

urlpatterns = [
    path('searchBook', views.landing_story8, name='searchBook'),
    path('hasil/', views.hasil, name='hasil'),
]
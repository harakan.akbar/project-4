from django.shortcuts import redirect, render
from django.http import JsonResponse
import requests
import  json

# Create your views here.

def landing_story8(request):
    if (request.user.is_anonymous):
        return redirect('/auth/login')

    return render (request,'main/story8.html')

def hasil(request):
    if (request.user.is_anonymous):
        return redirect('/auth/login')
        
    url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET.get('q','frozen')
    ret = requests.get(url)
    data= json.loads(ret.content)
    return JsonResponse(data, safe=False)
